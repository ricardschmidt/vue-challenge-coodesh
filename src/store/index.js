import { createStore } from 'vuex'
import { request } from './request.module';

export default new createStore({
	modules: {
		request
	  }
})

