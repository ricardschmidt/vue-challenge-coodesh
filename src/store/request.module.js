import RequestService from '../services/request.service';
import { getUsers } from '../services/request'

export const request = {
	namespaced: true,
	state: {
		users: getUsers() ? getUsers() : []
	},
	actions: {
		requestUsers({ commit }) {
			return RequestService.getRandomUsers()
			.then(() => {
					commit('setUsers', getUsers());
					return Promise.resolve(getUsers());
				},
			);
		},
	},
	mutations: {
		setUsers(state, users) {
			state.users = users;
		},
	}
};
