import { createWebHistory, createRouter } from "vue-router";
import Index from "../views/Index.vue";
import MainNavbar from "../layout/MainNavbar.vue";
import MainFooter from "../layout/MainFooter.vue";

const routes = [
    {
      path: "/",
      name: "index",
      components: { default: Index, header: MainNavbar, footer: MainFooter },
    },
    {
      path: "/:id",
      name: "user-card",
      components: { default: Index, header: MainNavbar, footer: MainFooter },
    },
];

const router = createRouter({
	history: createWebHistory(),
	routes,
});

  export default router;
