import axios from './axios';
import { setUsers } from './request'

class AuthService {
	getRandomUsers() {
		return axios.get('/', {
			params: {
				results: 50
			}
		}).then(response => {
			if (response.data) {
				setUsers(response.data.results);
			}
		})
	}
}
export default new AuthService();
