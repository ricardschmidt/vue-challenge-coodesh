export const USERS = "users";

export const getUsers = () => JSON.parse(localStorage.getItem(USERS));

export const setUsers = users => {
	localStorage.setItem(USERS, JSON.stringify(users));
};
