# Vue Challenge Coodesh in Vue 3 + Vite

Front para consulta da api RandomUser para a Pharma Inc com listagem, filtragem e expansão dos dados.

## Quick start

Quick start options:

- Clone the repo: `git clone https://gitlab.com/ricardschmidt/vue-challenge-coodesh.git`.
- [Download from Gitlab](https://gitlab.com/ricardschmidt/vue-challenge-coodesh/-/archive/main/vue-challenge-coodesh-main.zip).
- Run `npm install` or `yarn install`
- Run `npm run dev` or `yarn dev` to start a local development server


## Tecnologias usadas

[Vue.js (3)](https://vuejs.org/) as framework for development.
[Vue CLI 5.0.4](https://github.com/vuejs/vue-cli) for project scaffolding.
[Vue Router](https://router.vuejs.org/) for handling routes.
[Vuex](https://vuex.vuejs.org/) for state.
[Tailwind 3](https://tailwindcss.com/) as a general css foundation.
[Headless UI](https://headlessui.dev/) for some complex js components such as dialog, transitionChild.

## File Structure

Within the download you'll find the following directories and files:

```
Vue Challenge
|-- src
	|-- App.vue
	|-- main.js
	|-- index.css
	|-- assets
	|   |-- scss
	|-- components
	|-- layout
	|-- router
	|-- service
	|-- store
	|-- views
```

## Reference

Este é um projeto criado para um challenge by coodesh
